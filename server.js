// modules =================================================
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var opn = require('opn');
var cp = require('child_process');

// configuration ===========================================

// config files
var config = require('./config/config');

var port = process.env.PORT || 8080; // set our port
cp.execFile('\\site\\wwwroot\\MongoDB\\Server\\3.4\\bin\\mongod.exe', ['--dbpath', '\\site\\wwwroot\\startup\\statistics_db']);
setTimeout(function () {
    mongoose.connect(config.dbUrl); // connect to our mongoDB database (commented out after you enter in your own credentials)

    // get all data/stuff of the body (POST) parameters
    app.use(bodyParser.json()); // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded

    app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
    app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users

    app.use('/external', express.static(__dirname + '/node_modules'));
    app.use('/api/prediction', require('./app/controllers/predictionController'));
    app.use('/api/teams', require('./app/controllers/teams'));



    // start app ===============================================
    var server = app.listen(port);
    var io = require('socket.io').listen(server);

    // routes ==================================================
    require('./app/routes')(app, io); // pass our application into our routes

    console.log('Magic happens on port ' + port); // shoutout to the user
    exports = module.exports = app; // expose app

    var openBrowserForDebug = process.argv.indexOf('openBrowser') != -1;
    if (openBrowserForDebug) {
        opn('http://localhost:' + port + '/index.html');
    }
}, 7000);