// grab the mongoose module
var mongoose = require('mongoose');

var gameSchema = mongoose.Schema({
	homeTeamId : {type : String},
	homeTeam : {type : String},
	awayTeamId : {type : String},
	awayTeam : {type : String},
	homeScore : {type : String},
	awayScore : {type : String},
	gameTime : {type : String}
});

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Game', gameSchema);