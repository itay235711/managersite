// grab the mongoose module
var mongoose = require('mongoose');

var favoriteSchema = mongoose.Schema({
	teamId : {type : Number},
	name : {type : String},
	description : {type : String},
	user : {type : String},
});

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Favorite',favoriteSchema);