// grab the mongoose module
var mongoose = require('mongoose');

var teamSchema = mongoose.Schema({
	teamId : {type : Number},
	fullName : {type : String},
	triCode : {type : String},
	city : {type : String},
	confName : {type : String}, //conference- east\west
	nickname : {type : String}
});

// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Team',teamSchema);