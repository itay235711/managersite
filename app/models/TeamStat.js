// grab the mongoose module
var mongoose = require('mongoose');

var Stats = mongoose.Schema({
	team_id: { type: Number },
	gp: { type: Number }, //The number of games played
	w: { type: Number }, //The number of games won by a team
	l: { type: Number }, //The number of games lost by a team
	w_pct: { type: Number },
	min: { type: Number }, //The number of minutes played by a player
	fgm: { type: Number }, 
	fga: { type: Number },
	fg_pct: { type: Number },
	fg3m: { type: Number }, //The percentage of team field goals made by a player
	fg3a: { type: Number },  //The percentage of player or team field goals attempts that were three-point attempts
	fg3_pct: { type: Number },
	ftm: { type: Number }, // The percentage of team free throws made by a player
	fta: { type: Number }, //The percentage of team free throws attempted by a player
	ft_pct: { type: Number },
	oreb: { type: Number }, //(Offensive Rebounds) The number of rebounds collected by the team that attempted the shot
	dreb: { type: Number }, //(Percentage of Defensive Rebounds) The percentage of team defensive rebounds grabbed by a player
	reb: { type: Number },
	ast: { type: Number }, //The number of assists -- passes that lead directly to a made basket -- by a player or team
	tov: { type: Number },
	stl: { type: Number }, //The number of steals by a player or team
	blk: { type: Number }, //The number of shot attempts that are blocked by a player or team
	blka: { type: Number }, //The number of shots attempted by a player or team that are blocked by a defender
	pf: { type: Number }, // The percentage of team personal fouls by a player
	pfd: { type: Number }, // The number of personal fouls that are committed against a single player
	pts: { type: Number },
	plus_minus: { type: Number },
	gp_rank: { type: Number },
	w_rank: { type: Number },
	l_rank: { type: Number },
	w_pct_rank: { type: Number },
	min_rank: { type: Number },
	fgm_rank: { type: Number },
	fga_rank: { type: Number },
	fg_pct_rank: { type: Number },
	fg3m_rank: { type: Number },
	fg3a_rank: { type: Number },
	fg3_pct_rank: { type: Number },
	ftm_rank: { type: Number },
	fta_rank: { type: Number },
	ft_pct_rank: { type: Number },
	oreb_rank: { type: Number },
	dreb_rank: { type: Number },
	reb_rank: { type: Number },
	ast_rank: { type: Number },
	tov_rank: { type: Number },
	stl_rank: { type: Number },
	blk_rank: { type: Number },
	blka_rank: { type: Number },
	pf_rank: { type: Number },
	pfd_rank: { type: Number },
	pts_rank: { type: Number },
	plus_minus_rank: { type: Number }
});

var teamStatSchema = mongoose.Schema({
	Overall : [Stats]
}, { collection: 'lineups' });

// define our nerd model
var TeamStat = mongoose.model('TeamStat', teamStatSchema);

// module.exports allows us to pass this to other files when it is called
module.exports = TeamStat;