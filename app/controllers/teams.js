require('../models/Team');

var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Team = mongoose.model('Team');

router.get('/', function(req, res) {
    Team.find(function(err, teams) {
        if (err)
            res.send(err);

        res.json(teams);
    });
});

router.get('/search', function(req, res) {
    var searchQuery = {};
    if (req.query.name) {
        searchQuery.fullName = req.query.name;
    }
    if (req.query.city) {
        searchQuery.city = req.query.city;
    }
    if (req.query.confName) {
        searchQuery.confName = req.query.confName;
    }
    if (req.query.id) {
        searchQuery._id = req.query.id;
    }

    // use mongoose to get all teams in the database
    Team.find(searchQuery, function(err, teams) {
        // if there is an error retrieving, send the error. 
        if (err)
            res.send(err);

        res.json(teams); // return all teams in JSON format
    });
});

router.get('/missingPlayers/:teamId', function(req, res) {
    var teamId = req.params.teamId;

    //TODO: get from db
    var players = [{ name: 'omer', id: '1' }, { name: 'itay', id: '2' }];
    res.send(players);
});

module.exports = router;