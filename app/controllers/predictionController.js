var express = require('express'),
    request = require('request'),
    config = require('../../config/config'),
    router = express.Router();


router.post('/predictGame', function(req, res) {
    var homeTeam = req.params.homeTeam;
    var awayTeam = req.params.awayTeam;
    var homeInjuredPlayers = req.params.homeInjuredPlayers;
    var awayInjuredPlayers = req.params.awayInjuredPlayers;

    const options = {
        url: config.apiPredictUrl,
        params: {
            games: [{
                HomeTeam: homeTeam,
                AwayTeam: awayTeam,
                HomeInjuredPlayers: homeInjuredPlayers,
                AwayInjuredPlayers: awayInjuredPlayers
            }]
        },
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'User-Agent': 'my-reddit-client'
        }
    };

    request(options, function(err, res, body) {
        let json = JSON.parse(body);
        console.log(json);
    });

    if (req.params.homeTeam && req.params.awayTeam) {
        // TODO: calc in db     
    }

    res.send(req.query.homeTeam);
})


module.exports = router;