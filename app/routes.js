require('./models/Team');
require('./models/TeamStat');
require('./models/Favorite');
require('./models/Game');
var mongoose = require('mongoose');

var Team = mongoose.model("Team")
var TeamStat = mongoose.model("TeamStat")
var Favorite = mongoose.model("Favorite")
var Game = mongoose.model("Game")

module.exports = function(app, io) {

    // server routes ===========================================================
    // handle things like api calls
    // authentication routes
    app.get('/api/favorites', function(req, res) {
        Favorite.find({ user: req.query.user }, function(err, favorites) {
            if (err)
                res.send(err);

            res.json(favorites);
        });
    });

    app.post('/api/favorites', function(req, res) {
        var favorite = new Favorite(req.body);

        favorite.save(function(err, savedFavorite) {
            if (err)
                res.send(err);

            res.json({ id: savedFavorite._id });
        });
    });

    app.delete('/api/favorites/:id', function(req, res) {
        Favorite.remove({ _id: req.params.id }, function(err) {
            if (err)
                res.send(err);
            res.status(200).json({ status: "ok" });
        });
    });

    app.put('/api/favorites', function(req, res) {
        var favorite = new Favorite(req.body);

        Favorite.findByIdAndUpdate(favorite._id, favorite, function(err, savedFavorite) {
            if (err)
                res.send(err);

            res.json({ id: savedFavorite._id });
        });
    });

    app.get('/api/teamStats/:id', function(req, res) {
        TeamStat.find({ Overall: { $elemMatch: { team_id: req.params.id } } }, function(err, teamStats) {
            if (err)
                res.send(err);
            else if (teamStats.length == 0)
                res.status(404)
                    .send('No stats where found for the team provided');
            else
                res.json(teamStats[0].Overall[0]);
        });
    });

    app.get('/api/teamStats/groupedGames/:id', function(req, res) {
        Game.aggregate([{
                $match: {
                    homeTeamId: req.params.id
                }
            },
            {
                $group: {
                    _id: '$awayTeam',
                    count: { $sum: 1 }
                }
            }
        ], function(err, result) {
            if (err) {
                res.send(err);
            } else {
                res.json(result);
            }
        });
    });

    app.get('/api/games/search', function(req, res) {
        var searchQuery = {};
        if (req.query.homeTeam) {
            searchQuery.homeTeamId = req.query.homeTeam;
        }
        if (req.query.awayTeam) {
            searchQuery.awayTeamId = req.query.awayTeam;
        }
        if (req.query.homeScore) {
            searchQuery.homeScore = req.query.homeScore;
        }

        // use mongoose to get all teams in the database
        Game.find(searchQuery, function(err, games) {
            // if there is an error retrieving, send the error. 
            if (err)
                res.send(err);

            res.json(games); // return all teams in JSON format
        });
    });

    app.get('/api/userLogin', function(req, res) {
        io.emit('broadcast', { val: req.query.user });
        res.send();
    });

    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html');
    });
};