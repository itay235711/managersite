/**
 * Created by itay on 3/8/2017.
 */
mainApp.service('mockingService', function () {

    var seasonMocks = {
        getCurrentSeasonDetails : getCurrentSeasonDetails,
        getCurrentSeasonStats : getCurrentSeasonStats
    };

    var teamMocks = {
        getAllTeamsList : getAllTeamsList
    };

    var self = {
        season : seasonMocks,
        team : teamMocks
    };

    function getCurrentSeasonDetails() {
        return {
            seasonName : '2016-17 crazy league',
            seasonType : 'Regular Season',
            perMode : 'Per Game',
            lineupsType : '5 Man Lineup'
        }
    }

    function getCurrentSeasonStats() {
        return {
            fieldsDisplayNames: ['LINEUP', 'TEAM', 'GP', 'MIN', 'PTS', 'FGM'],
            statsItems: [
            { lineup:'asdsaasfasf, xcvdsfasda, csdagasgag, asfgfsad, aszxcsdf', team:'WAS', gp:1, min:22.1, pts:44.0, fgm:16.0 },
            { lineup:'xxxxxxxxxxx, yyyyyyyyyy, zzzzzzzzzz, aaaaaaaa, bbbbbbbb', team:'MIN', gp:1, min:22.1, pts:44.0, fgm:16.0 },
            { lineup:'aaaaaaaaaaa, yyyyyyyyyy, zzzzzzzzzz, aaaaaaaa, bbbbbbbb', team:'WAS', gp:1, min:22.1, pts:44.0, fgm:16.0 },
            { lineup:'bbbbbbbbbbb, yyyyyyyyyy, zzzzzzzzzz, aaaaaaaa, bbbbbbbb', team:'LAC', gp:1, min:22.1, pts:44.0, fgm:16.0 },
            { lineup:'aabbbbbaaaa, yyyyyyyyyy, zzzzzzzzzz, aaaaaaaa, bbbbbbbb', team:'PHX', gp:1, min:22.1, pts:44.0, fgm:16.0 },
            { lineup:'bbbbbbaaaaa, yyyyyyyyyy, zzzzzzzzzz, aaaaaaaa, bbbbbbbb', team:'IND', gp:1, min:22.1, pts:44.0, fgm:16.0 }
        ]};
    }

    function getAllTeamsList() {
        return ['Team a','Team b', 'Team c'];
    }
    
    return self;
});