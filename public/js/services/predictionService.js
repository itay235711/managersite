mainApp.service('predictionService', ['$http', '$rootScope', function($http, $rootScope) {
    var self = {
        getGameResult: getGameResult
    };

    function getGameResult(homeTeam, homeMissingPlayers, awayTeam, awayMissingPlayers) {
        var promise = $http({
                url: 'api/prediction/predictGame',
                method: "POST",
                params: {
                    homeTeam: homeTeam,
                    homeMissingPlayers: homeMissingPlayers,
                    awayMissingPlayers: awayMissingPlayers,
                    awayTeam: awayTeam
                }
            })
            .then(delayPromise(4000))
            .then(function(response) {
                return (response.data);
            }, function(error) {
                console.log(error);
            });

        return promise;
    }

    function delayPromise(delay) {
        //return a function that accepts a single variable
        return function(data) {
            //this function returns a promise.
            return new Promise(function(resolve, reject) {
                setTimeout(function() {
                    //a promise that is resolved after "delay" milliseconds with the data provided
                    resolve(data);
                }, delay);
            });
        }
    }

    return self;
}]);