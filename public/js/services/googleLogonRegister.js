mainApp.run(["$rootScope", "$http", function($rootScope, $http) {
    window.onSignin = function(googleUser) {
        // todo think of better way to pass google user
        window.googleUser = googleUser.getBasicProfile();
        $rootScope.$broadcast('userLogin', window.googleUser.ig);

        var promise = $http({
            url: 'api/userLogin',
            method: "GET",
            params: { user: window.googleUser.ig }
        }).then(function(response) {
            console.log(response.data);
        }, function(error) {
            console.log(error);
        });

        return promise;
    };

    window.signOut = function() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function() {
            console.log('User signed out.');
        });

        window.googleUser = undefined;
        $rootScope.$broadcast('userSignout');
    }
}]);