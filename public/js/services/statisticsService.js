/**
 * Created by itay on 3/8/2017.
 */
mainApp.service('statisticsService', ['$http', '$rootScope', function ($http, $rootScope) {

    var self = {
        getStatistics : getStatistics,
        subscribeToStatisticReceived : subscribeToStatisticReceived,
        subscribeToStatisticErrorReceived : subscribeToStatisticErrorReceived,
        getTeamGamesGrouped : getTeamGamesGrouped
    };

    function getTeamGamesGrouped(teamId) {
        var promise = $http({
            url: '/api/teamStats/groupedGames/' + teamId,
            method: "GET",
        }).then(function (response) {
            return response.data;
        }, function (error) {
            alert(error);
        });

        return promise;
    }

    function getStatistics(teamId) {
        var promise = $http({
            url: '/api/teamStats/' + teamId,
            method: "GET"
        }).then(function (response) {
            notifyStatisticReceived(response.data);
            return response.data;
        }, function (error) {
            notifyStatisticErrorReceived(error.data)
        });

        return promise;
    }

    function subscribeToStatisticReceived(scope, callback) {
        var handler = $rootScope.$on('statistic-received', callback);
        scope.$on('$destroy', handler);
    }

    function subscribeToStatisticErrorReceived(scope, callback) {
        var handler = $rootScope.$on('statistic-error-received', callback);
        scope.$on('$destroy', handler);
    }

    function notifyStatisticReceived(statistic) {
        $rootScope.$emit('statistic-received', statistic);
    }


    function notifyStatisticErrorReceived(statistic) {
        $rootScope.$emit('statistic-error-received', statistic);
    };

    return self;
}]);