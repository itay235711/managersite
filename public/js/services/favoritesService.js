/**
 * Created by itay on 3/8/2017.
 */
mainApp.service('favoritesService', ['$http', '$rootScope', function ($http, $rootScope) {

    var self = {
        getFavorites: getFavorites,
        subscribeToNewFavorite: subscribeToNewFavorite,
        addFavorite: addFavorite,
        deleteFavorite : deleteFavorite,
        updateFavorite : updateFavorite
    };

    function addFavorite(favorite) {
        $http.post('api/favorites/', favorite).then(function (response) {
            favorite._id = response.data.id;
            notifyFavoriteAdded(favorite);
        }, function (error) {
            alert(error);
        });
    };

    function deleteFavorite(id) {
        var promise = $http({
            method: 'DELETE',
            url: '/api/favorites/' + id
        }).then(function (response) {
            return;
        }, function (error) {
            alert(error);
        });

        return promise;
    };
    
    function updateFavorite(favorite) {
        var promise = $http.put('api/favorites/', favorite).then(function (response) {
            return;
        }, function (error) {
            alert(error);
        });

        return promise;
    };

    function getFavorites() {
        var promise = $http({
            url: 'api/favorites/',
            method: "GET",
            params: { user: window.googleUser.getName() }
        }).then(function (response) {
            return response.data;
        }, function (error) {
            alert(error);
        });

        return promise;
    };

    function subscribeToNewFavorite(scope, callback) {
        var handler = $rootScope.$on('favorite-added', callback);
        scope.$on('$destroy', handler);
    };

    function notifyFavoriteAdded(favorite) {
        $rootScope.$emit('favorite-added', favorite);
    };

    return self;
}]);