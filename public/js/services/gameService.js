/**
 * Created by itay on 3/8/2017.
 */
mainApp.service('gameService', ['$http', '$rootScope', function($http, $rootScope) {

    var self = {
        searchGame: searchGame,
        subscribeToSearchResults: subscribeToSearchResults
    };

    function searchGame(homeTeam, awayTeam, homeScore) {
        var promise = $http({
            url: 'api/games/search',
            method: "GET",
            params: { homeTeam: homeTeam, awayTeam: awayTeam, homeScore: homeScore }
        }).then(function(response) {
            notify(response.data);
        }, function(error) {
            console.log(error);
        });
    }

    function subscribeToSearchResults(scope, callback) {
        var handler = $rootScope.$on('game-search-result-changed', callback);
        scope.$on('$destroy', handler);
    }

    function notify(games) {
        $rootScope.$emit('game-search-result-changed', games);
    }

    return self;
}]);