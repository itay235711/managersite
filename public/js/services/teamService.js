mainApp.service('teamService', ['$http', '$rootScope', function($http, $rootScope) {

    var self = {
        searchTeam: searchTeam,
        subscribeToSearchResults: subscribeToSearchResults,
        getAllTeams: getAllTeams,
        getMissingPlayers: getMissingPlayers,
        get: get
    };

    function searchTeam(name, city, confName) {
        var promise = $http({
            url: 'api/teams/search',
            method: "GET",
            params: { name: name, city: city, confName: confName }
        }).then(function(response) {
            notify(response.data);
        }, function(error) {
            console.log(error);
        });
    }


    function get(id) {
        var promise = $http({
            url: 'api/teams/search',
            method: "GET",
            params: { id: id }
        }).then(function(response) {
            notify(response.data);
        }, function(error) {
            console.log(error);
        });
    }

    function subscribeToSearchResults(scope, callback) {
        var handler = $rootScope.$on('team-search-result-changed', callback);
        scope.$on('$destroy', handler);
    }

    function notify(teams) {
        $rootScope.$emit('team-search-result-changed', teams);
    }

    function getAllTeams() {
        var promise = $http({
            url: 'api/teams/',
            method: "GET"
        }).then(function(response) {
            return (response.data);
        }, function(error) {
            console.log(error);
        });

        return promise;
    }

    function getMissingPlayers(teamId) {
        var promise = $http({
            url: 'api/teams/missingPlayers/' + teamId,
            method: "GET"
        }).then(function(response) {
            return (response.data);
        }, function(error) {
            console.log(error);
        });

        return promise;
    }

    return self;
}]);