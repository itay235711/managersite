mainApp.controller('GamePredictionController', ['predictionService', 'teamService', '$scope',

    function(predictionService, teamService, $scope) {
        $scope.winnerTeam = null;
        $scope.homeTeamMissingPlayers = [];
        $scope.awayTeamMissingPlayers = [];
        $scope.currHomeMissingPlayer = [];
        $scope.currAwayMissingPlayer = [];

        $scope.loadTeams = function() {
            $scope.homeTeamScore = null;
            $scope.awayTeamScore = null;
            $scope.pendingRequest = false;

            if (!$scope.allTeams) {
                teamService.getAllTeams().then(function(teams) {
                    $scope.allTeams = teams;
                });
            } else {
                $scope.winnerTeam = null;
            }
        };

        $scope.predict = function() {
            var homeTeam = $scope.homeTeam;
            var awayTeam = $scope.awayTeam;
            var homeInjuredPlayers = $scope.getInjuredPlayersNames($scope.currHomeMissingPlayer);
            var awayInjuredPlayers = $scope.getInjuredPlayersNames($scope.currAwayMissingPlayer);

            $scope.winnerTeam = null;
            $scope.pendingRequest = true;

            predictionService.getGameResult(homeTeam.fullName, homeInjuredPlayers,
                    awayTeam.fullName, awayInjuredPlayers)
                .then(function(result) {
                    var winnerTeamName = $scope.getWinnerTeamIdFromRes(result);
                    if (winnerTeamName == homeTeam.fullName)
                        $scope.winnerTeam = homeTeam;
                    else {
                        $scope.winnerTeam = awayTeam;
                    }

                    $scope.pendingRequest = false;
                })
        };

        $scope.getWinnerTeamIdFromRes = function(result) {
            var firstGame = result[0];
            var predict = firstGame.PredictedResult;
            var winnerTeam;

            if (predict === 1) {
                winnerTeam = firstGame.HomeTeamName;
            } else {
                winnerTeam = firstGame.AwayTeamName;
            }

            return winnerTeam;
        }

        $scope.getInjuredPlayersNames = function(injuredPlayers) {
            var playersName = [];
            injuredPlayers.forEach(function(currPlayer) {
                playersName.push(currPlayer.fullName);
            }, this);

            return playersName;
        }

        $scope.isEmpty = function(obj) {
            if (obj) {
                return false;
            }
            return true;
        };

        $scope.disabledPredictButton = function() {
            if ($scope.homeTeam && $scope.awayTeam &&
                ($scope.homeTeam.teamId !== $scope.awayTeam.teamId)) {
                return false;
            }
            return true;
        }


        $scope.updateHomeMissingPlayers = function(players) {
            $scope.currHomeMissingPlayer = players;
        }

        $scope.updateAwayMissingPlayers = function(players) {
            $scope.currAwayMissingPlayer = players;
        }

        $scope.setHomePlayers = function() {
            if ($scope.homeTeam) {
                teamService.getMissingPlayers($scope.homeTeam.teamId).then(function(missingPlayers) {
                    $scope.homeTeamMissingPlayers = missingPlayers;
                });
            } else {
                $scope.homeTeamMissingPlayers = [];
            }
        }

        $scope.setAwayPlayers = function() {
            if ($scope.awayTeam) {
                teamService.getMissingPlayers($scope.awayTeam.teamId).then(function(missingPlayers) {
                    $scope.awayTeamMissingPlayers = missingPlayers;
                });
            } else {
                $scope.awayTeamMissingPlayers = [];
            }
        }
    }
]);