/**
 * Created by itay on 3/8/2017.
 */

mainApp.controller('SeasonStatisticsController', ['mockingService', 'statisticsService', '$location','$scope', function (mockingService, statisticsService,$location, $scope) {
    $scope.seasonDetails = mockingService.season.getCurrentSeasonDetails();
    $scope.seasonStats = mockingService.season.getCurrentSeasonStats();

    var teamIdParam = $location.search().teamId;
    if(teamIdParam) {
        $scope.mode="Team";
        statisticsService.getStatistics(teamIdParam).then(function(){
            $scope.teamsList = null;
            $scope.teamIdParam = teamIdParam;
        });
    }
}]);