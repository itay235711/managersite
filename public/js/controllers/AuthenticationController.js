mainApp.controller('AuthenticationController', ['$scope', '$rootScope', '$http', 'toastr', function($scope, $rootScope, $http, toastr) {
    $scope.isAuthenticated = false;
    // toastr.options.timeOut = 2;

    $rootScope.$on('userLogin', function(event, user) {
        $scope.isAuthenticated = true;
        toastr.info('Welcome ' + user + '!', 'User Logon');
        $scope.$apply();
    });

    $rootScope.$on('userSignout', function() {
        $scope.isAuthenticated = false;
        $scope.$apply();
    });
}]);