mainApp.config(['$routeProvider', '$locationProvider', 'toastrConfig', function($routeProvider, $locationProvider, toastrConfig) {

    $routeProvider

    // home page
        .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeController'
    })

    .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeController'
    })

    .when('/statistics', {
        templateUrl: 'views/seasonStatistics.html',
        controller: 'SeasonStatisticsController'
    })

    .when('/gamePrediction', {
        templateUrl: 'views/gamePrediction.html',
        controller: 'GamePredictionController'
    })

    .when('/favoriteTeams', {
        templateUrl: 'views/favoriteTeams.html',
        controller: 'FavoriteTeamsController'
    });

    $locationProvider.html5Mode(true);

    angular.extend(toastrConfig, {
        timeOut : 1500,
        positionClass: "toast-bottom-left"
    });
}]);