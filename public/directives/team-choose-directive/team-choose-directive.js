/**
 * Created by itay on 3/8/2017.
 */
mainApp.directive('teamChooseDirective', ['teamService', 'statisticsService', function (teamService, statisticsService) {
    return {
        restrict: 'E',
        templateUrl: 'directives/team-choose-directive/team-choose.html',
        link: function ($scope) {

            teamService.getAllTeams().then(function (teams) {
                $scope.allTeams = teams;

                if ($scope.teamIdParam) {
                    setSelectedTeam(teams, $scope.teamIdParam);
                }
            });

            $scope.selectChanged = function(){
                statisticsService.getStatistics($scope.selectedTeam.teamId);
            };

            function setSelectedTeam(teams, teamId) {
                $scope.selectedTeam = teams.find(function (t) {
                    return t.teamId == teamId;
                });
                $scope.teamIdParam = undefined;
            }
        }
    }
}]);
