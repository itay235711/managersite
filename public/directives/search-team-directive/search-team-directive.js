/**
 * Created by itay on 3/8/2017.
 */
mainApp.directive('searchTeamDirective', ['teamService', function (teamService) {
    return {
        restrict: 'E',
        templateUrl: 'directives/search-team-directive/search-team.html',
        link: function (scope) {
            scope.search = function () {
                teamService.searchTeam(scope.searchParams.fullName,
                    scope.searchParams.city, scope.searchParams.confName);
            };
        }
    }
}]);
