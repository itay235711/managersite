/**
 * Created by itay on 3/8/2017.
 */

mainApp.directive('seasonDetailsDirective', function () {
   return {
       restrict: 'E',
       templateUrl: 'directives/season-details-directive/season-details.html'
   }
});