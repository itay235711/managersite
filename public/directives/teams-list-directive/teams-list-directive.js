/**
 * Created by itay on 3/8/2017.
 */
mainApp.directive('teamsListDirective', ['teamService', 'favoritesService', 'statisticsService', function (teamService, favoritesService, statisticsService) {
    return {
        restrict: 'E',
        templateUrl: 'directives/teams-list-directive/teams-list-directive.html',
        scope : {mode : "@mode"},
        link: function (scope) {
            teamService.subscribeToSearchResults(scope, function (event, teams) {
                scope.teamsList = teams;
            });
            
            scope.addToFavorites = function(team) {
                var favorite = {
                    teamId : team.teamId,
                    name : team.fullName,
                    description : team.fullName,
                    user : window.googleUser.getName()
                };

                favoritesService.addFavorite(favorite);
            }

            scope.showStatistics = function(team) {
                statisticsService.getStatistics(team.teamId).then(function(){
                    scope.teamsList = null;
                });
            }
        }
    }
}]);
