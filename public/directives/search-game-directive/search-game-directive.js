/**
 * Created by itay on 3/8/2017.
 */
mainApp.directive('searchGameDirective', ['gameService', 'teamService', function (gameService, teamService) {
    return {
        restrict: 'E',
        templateUrl: 'directives/search-game-directive/search-game-directive.html',
        link: function (scope) {
            teamService.getAllTeams().then(function (teams) {
                scope.allTeams = teams;
            });

            scope.searchGame = function () {
                homeTeamId = null;
                awayTeamId = null;
                if (scope.searchParams.homeTeam)
                    homeTeamId = scope.searchParams.homeTeam.teamId;
                if (scope.searchParams.awayTeam)
                    awayTeamId = scope.searchParams.awayTeam.teamId;
                gameService.searchGame(homeTeamId,
                    awayTeamId, scope.searchParams.homeScore);
            };
        }
    }
}]);
