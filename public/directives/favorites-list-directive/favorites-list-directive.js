/**
 * Created by itay on 3/8/2017.
 */
mainApp.directive('favoritesListDirective', ['favoritesService', '$location', function (favoritesService, $location) {
    return {
        restrict: 'E',
        templateUrl: 'directives/favorites-list-directive/favorites-list-directive.html',
        link: function (scope) {
            favoritesService.getFavorites().then(function (favorites) {
                scope.favorites = favorites;
            });
            favoritesService.subscribeToNewFavorite(scope, function (event, favorite) {
                scope.favorites.push(favorite);
            });

            scope.deleteFavorite = function (favorite, index) {
                favoritesService.deleteFavorite(favorite._id).then(function () {
                    scope.favorites.splice(index, 1);
                });
            };
            
            scope.showStatistics = function (favorite) {
                $location.url("/statistics?teamId=" + favorite.teamId);
            };

            scope.editItem = function (item) {
                item.editing = true;
            };

            scope.doneEditing = function (item) {
                item.editing = false;
                favoritesService.updateFavorite(item);
            };
        }
    }
}]);
