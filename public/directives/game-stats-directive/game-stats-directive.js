/**
 * Created by itay on 3/8/2017.
 */

mainApp.directive('gameStatsDirective', ['gameService', function (gameService) {
    return {
        restrict: 'E',
        templateUrl: 'directives/game-stats-directive/game-stats.html',
        link: function (scope) {
            gameService.subscribeToSearchResults(scope, function (event, games) {
                scope.games = games;
            });
        }
    }
}]);